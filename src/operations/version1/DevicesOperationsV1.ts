let _ = require('lodash');
let async = require('async');

import { DeviceV1, IDevicesClientV1 } from 'ad-board-clients-devices-node';
import { IReferences } from 'pip-services3-commons-node';
import { Descriptor } from 'pip-services3-commons-node';
import { RestOperations } from 'pip-services3-rpc-node';


export class DevicesOperationsV1 extends RestOperations {
    private _devicesClient: IDevicesClientV1;
    private _correlationId = "devices-operations";

    public constructor() {
        super();

        this._dependencyResolver.put('devices-client', new Descriptor('ad-board-devices', 'client', '*', '*', '1.0'));
    }

    public setReferences(references: IReferences): void {
        super.setReferences(references);

        this._devicesClient = this._dependencyResolver.getOneRequired<IDevicesClientV1>('devices-client');
    }

    public getDevices(req: any, res: any): void {
        let filter = this.getFilterParams(req);
        let paging = this.getPagingParams(req);

        this._devicesClient.getDevices(
            this._correlationId, filter, paging, this.sendResult(req, res)
        );
    }

    public getDeviceCode(req: any, res: any): void {
        let devId = req.route.params.dev_id;

        this._devicesClient.generateRequstCode(
            this._correlationId, devId, (err, code) => {
                this.sendResult(req, res)(err, { code: code })
            }
        );
    }

    public getDevice(req: any, res: any): void {
        let devId = req.route.params.dev_id;

        this._devicesClient.getDeviceById(
            this._correlationId, devId, this.sendResult(req, res)
        );
    }

    public createDevice(req: any, res: any): void {
        let dev: DeviceV1 = req.body || {};

        dev.last_connection = new Date(dev.last_connection);

        this._devicesClient.createDevice(
            this._correlationId, dev, this.sendResult(req, res)
        );
    }

    public updateDevice(req: any, res: any): void {
        let devId = req.route.params.dev_id;
        let dev: DeviceV1 = req.body || {};
        dev.id = devId;
        dev.last_connection = new Date(dev.last_connection) || new Date();

        this._devicesClient.updateDevice(
            this._correlationId, dev, this.sendResult(req, res)
        );
    }

    public deleteDevice(req: any, res: any): void {
        let devId = req.route.params.dev_id;

        this._devicesClient.deleteDeviceById(
            this._correlationId, devId, this.sendResult(req, res)
        );
    }

}