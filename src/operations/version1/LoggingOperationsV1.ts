let _ = require('lodash');
let async = require('async');

import { ConfigParams } from 'pip-services3-commons-node';
import { IReferences } from 'pip-services3-commons-node';
import { Descriptor } from 'pip-services3-commons-node'; 
import { DependencyResolver } from 'pip-services3-commons-node';
import { LogLevelConverter } from 'pip-services3-components-node';
import { StringConverter } from 'pip-services3-commons-node';

import { ILoggingClientV1 } from 'pip-clients-logging-node';
import { LogMessageV1 } from 'pip-clients-logging-node';

import { RestOperations } from 'pip-services3-rpc-node';

export class LoggingOperationsV1  extends RestOperations {
    private _loggingClient: ILoggingClientV1;

    public constructor() {
        super();

        this._dependencyResolver.put('logging', new Descriptor('pip-services-logging', 'client', '*', '*', '1.0'));
    }

    public setReferences(references: IReferences): void {
        super.setReferences(references);

        this._loggingClient = this._dependencyResolver.getOneRequired<ILoggingClientV1>('logging');
    }

    public getMessages(req: any, res: any): void {
        let filter = this.getFilterParams(req);
        let paging = this.getPagingParams(req);

        this._loggingClient.readMessages(
            null, filter, paging, this.sendResult(req, res)
        );
    }

    public getErrors(req: any, res: any): void {
        let filter = this.getFilterParams(req);
        let paging = this.getPagingParams(req);

        this._loggingClient.readErrors(
            null, filter, paging, this.sendResult(req, res)
        );
    }

    private messageToText(message: LogMessageV1): string {
        let output = "["
            + (message.correlation_id || "---")
            + ":"
            + LogLevelConverter.toString(message.level)
            + ":"
            + StringConverter.toString(message.time)
            + "] "
            + message.message;

        if (message.error != null) {
            if (message.message == "")
                output += "Error: ";
            else output += ": ";
        
            output += message.error.type
                + " Code: " + message.error.code
                + " Message: " + message.error.message
                + " StackTrace: " + message.error.stack_trace;
        }

        return output;
    }

    private messagesToText(messages: LogMessageV1[]): string {
        if (messages == null) return null;

        let output = "";
        _.each(messages, (m) => {
            if (output.length > 0) output += "\r\n";
            output += this.messageToText(m);
        });
        return output;
    }

    public getMessagesAsText(req: any, res: any): void {
        let filter = this.getFilterParams(req);
        let paging = this.getPagingParams(req);

        this._loggingClient.readMessages(
            null, filter, paging, (err, page) => {
                if (err != null) this.sendError(req, res, err);
                else res.send(this.messagesToText(page.data));
            }
        );
    }

    public getErrorsAsText(req: any, res: any): void {
        let filter = this.getFilterParams(req);
        let paging = this.getPagingParams(req);

        this._loggingClient.readErrors(
            null, filter, paging, (err, page) => {
                if (err != null) this.sendError(req, res, err);
                else res.send(this.messagesToText(page.data));
            }
        );
    }

    public writeMessage(req: any, res: any): void {
        let message = req.body;

        this._loggingClient.writeMessage(
            null, message, this.sendResult(req, res)
        );
    }

    public clearMessages(req: any, res: any): void {
        this._loggingClient.clear(
            null, (err) => {
                if (err) this.sendError(req, res, err);
                else res.json(204);
            }
        );
    }

}