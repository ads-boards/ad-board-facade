let _ = require('lodash');
let async = require('async');

import { AdV1, IAdsClientV1 } from 'ad-board-clients-ads-node';
import { IDevicegroupsClientV1 } from 'ad-board-clients-devicegroups-node';
import { DeviceV1, IDevicesClientV1 } from 'ad-board-clients-devices-node';
import { FilterParams, IReferences, NotFoundException } from 'pip-services3-commons-node';
import { Descriptor } from 'pip-services3-commons-node';
import { RestOperations } from 'pip-services3-rpc-node';


export class BoardsOperationsV1 extends RestOperations {
    private _adsClient: IAdsClientV1;
    private _devicegroupsClient: IDevicegroupsClientV1;
    private _devicesClient: IDevicesClientV1;

    private _correlationId = "boards-operations";

    public constructor() {
        super();

        this._dependencyResolver.put('ads-client', new Descriptor('ad-board-ads', 'client', '*', '*', '1.0'));
        this._dependencyResolver.put('devices-client', new Descriptor('ad-board-devices', 'client', '*', '*', '1.0'));
        this._dependencyResolver.put('device_groups-client', new Descriptor('ad-board-devicegroups', 'client', '*', '*', '1.0'));
    }

    public setReferences(references: IReferences): void {
        super.setReferences(references);

        this._adsClient = this._dependencyResolver.getOneRequired<IAdsClientV1>('ads-client');
        this._devicesClient = this._dependencyResolver.getOneRequired<IDevicesClientV1>('devices-client');
        this._devicegroupsClient = this._dependencyResolver.getOneRequired<IDevicegroupsClientV1>('device_groups-client');
    }

    public getAds(req: any, res: any): void {
        let reqId = req.route.params.dev_id;

        let result: Array<AdV1> = [];
        let device: DeviceV1;
        let groups: string[];

        async.series([
            (callback) => {
                this._devicesClient.getDevices(this._correlationId, FilterParams.fromTuples("request_code", reqId), null, (err, page) => {
                    if (err) {
                        callback(err);
                        return;
                    }
                    if (page == null || page.data.length == 0) {
                        err = new NotFoundException(this._correlationId, "NOT_FOUND", "Device with request_code " + reqId + " not found")
                            .withDetails("request_code", reqId);
                        callback(err);
                        return;
                    }
                    device = page.data[0];
                    callback();
                })
            },
            // update device last connection 
            (callback) => {
                device.last_connection = new Date();

                this._devicesClient.updateDevice(this._correlationId, device, (err, item) => {
                    if (err) {
                        callback(err);
                        return;
                    }
                    if (item == null) {
                        err = new NotFoundException(this._correlationId, "NOT_FOUND", "Device with request_code " + reqId + " not found")
                            .withDetails("request_code", reqId);
                        callback(err);
                        return;
                    }
                    device = item;
                    callback();
                })
            },
            (callback) => {
                this._devicegroupsClient.getDevicegroups(this._correlationId, FilterParams.fromTuples("device_ids", [device.id]), null, (err, page) => {
                    if (err) {
                        callback(err);
                        return;
                    }
                    if (page == null || page.data.length == 0) {
                        err = new NotFoundException(this._correlationId, "NOT_FOUND", "Groups with device " + device.id + " not found")
                            .withDetails("device_id", device.id);
                        callback(err);
                        return;
                    }
                    groups = page.data.map((item) => { return item.id });
                    callback();
                })
            },
            (callback) => {
                this._adsClient.getAds(this._correlationId, FilterParams.fromTuples(
                    "publish_group_ids", groups,
                    //"deleted", false,
                    "disabled", false,
                    "end_time_from", new Date()
                ), null, (err, page) => {
                    if (err) {
                        callback(err);
                        return;
                    }
                    result = page.data;
                    callback();
                })
            }
        ], (err) => {
            this.sendResult(req, res)(err, { data: result });
        });
    }
}