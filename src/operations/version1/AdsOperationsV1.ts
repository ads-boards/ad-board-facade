let _ = require('lodash');
let async = require('async');

import { AdV1, IAdsClientV1 } from 'ad-board-clients-ads-node';
import { IReferences } from 'pip-services3-commons-node';
import { Descriptor } from 'pip-services3-commons-node';
import { RestOperations } from 'pip-services3-rpc-node';


export class AdsOperationsV1 extends RestOperations {
    private _adsClient: IAdsClientV1;
    private _correlationId = "ads-operations";

    public constructor() {
        super();

        this._dependencyResolver.put('ads-client', new Descriptor('ad-board-ads', 'client', '*', '*', '1.0'));
    }

    public setReferences(references: IReferences): void {
        super.setReferences(references);

        this._adsClient = this._dependencyResolver.getOneRequired<IAdsClientV1>('ads-client');
    }

    public getAds(req: any, res: any): void {
        let filter = this.getFilterParams(req);
        let paging = this.getPagingParams(req);

        this._adsClient.getAds(
            this._correlationId, filter, paging, this.sendResult(req, res)
        );
    }

    public getAd(req: any, res: any): void {
        let adId = req.route.params.ad_id;

        this._adsClient.getAdById(
            this._correlationId, adId, this.sendResult(req, res)
        );
    }

    public createAd(req: any, res: any): void {
        let ad: AdV1 = req.body || {};

        ad.create_time = new Date(ad.create_time) || new Date()
        ad.end_time = ad.end_time ? new Date(ad.end_time) : ad.end_time

        this._adsClient.createAd(
            this._correlationId, ad, this.sendResult(req, res)
        );
    }

    public updateAd(req: any, res: any): void {
        let adId = req.route.params.ad_id;
        let ad: AdV1 = req.body || {};

        ad.id = adId;

        ad.create_time = new Date(ad.create_time)
        ad.end_time = ad.end_time ? new Date(ad.end_time) : ad.end_time

        this._adsClient.updateAd(
            this._correlationId, ad, this.sendResult(req, res)
        );
    }

    public deleteAd(req: any, res: any): void {
        let adId = req.route.params.ad_id;

        this._adsClient.deleteAdById(
            this._correlationId, adId, this.sendResult(req, res)
        );
    }

}