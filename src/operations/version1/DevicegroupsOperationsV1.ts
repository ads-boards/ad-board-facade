let _ = require('lodash');
let async = require('async');


import { DevicegroupV1, IDevicegroupsClientV1 } from 'ad-board-clients-devicegroups-node';
import { IReferences } from 'pip-services3-commons-node';
import { Descriptor } from 'pip-services3-commons-node'; 
import { RestOperations } from 'pip-services3-rpc-node';


export class DevicegroupsOperationsV1  extends RestOperations {
    private _devicegroupsClient: IDevicegroupsClientV1;
    private _correlationId = "devicegroups-operations";

    public constructor() {
        super();

        this._dependencyResolver.put('device_groups-client', new Descriptor('ad-board-devicegroups', 'client', '*', '*', '1.0'));
    }

    public setReferences(references: IReferences): void {
        super.setReferences(references);

        this._devicegroupsClient = this._dependencyResolver.getOneRequired<IDevicegroupsClientV1>('device_groups-client');
    }

    public getDeviceGroups(req: any, res: any): void {
        let filter = this.getFilterParams(req);
        let paging = this.getPagingParams(req);

        this._devicegroupsClient.getDevicegroups(
            this._correlationId, filter, paging, this.sendResult(req, res)
        );
    }

    public getDeviceGroup(req: any, res: any): void {
        let groupId = req.route.params.group_id;

        this._devicegroupsClient.getDevicegroupById(
            this._correlationId, groupId, this.sendResult(req, res)
        );
    }

    public createDeviceGroup(req: any, res: any): void {
        let devgrp:DevicegroupV1 = req.body || {};

        this._devicegroupsClient.createDevicegroup(
            this._correlationId, devgrp, this.sendResult(req, res)
        );
    }

    public updateDeviceGroup(req: any, res: any): void {
        let groupId = req.route.params.group_id;
        let devgrp:DevicegroupV1 = req.body || {};
        devgrp.id = groupId;

        this._devicegroupsClient.updateDevicegroup(
            this._correlationId, devgrp, this.sendResult(req, res)
        );
    }

    public deleteDeviceGroup(req: any, res: any): void {
        let groupId = req.route.params.group_id;

        this._devicegroupsClient.deleteDevicegroupById(
            this._correlationId, groupId, this.sendResult(req, res)
        );
    }

}