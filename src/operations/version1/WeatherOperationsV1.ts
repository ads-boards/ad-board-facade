let _ = require('lodash');
let async = require('async');

import { YandexWeatherClientV1 } from 'ad-board-clients-yandexweather-node';
import { IReferences } from 'pip-services3-commons-node';
import { Descriptor } from 'pip-services3-commons-node';
import { RestOperations } from 'pip-services3-rpc-node';


export class WeatherOperationsV1 extends RestOperations {
    private _weatherClient: YandexWeatherClientV1;
    private _correlationId = "weather-operations";

    public constructor() {
        super();

        this._dependencyResolver.put('weather-client', new Descriptor('ad-board-yandexweather', 'client', '*', '*', '1.0'));
    }

    public setReferences(references: IReferences): void {
        super.setReferences(references);
        this._weatherClient = this._dependencyResolver.getOneRequired<YandexWeatherClientV1>('weather-client');
    }

    public getWeatherFromCache(req: any, res: any): void {
        this._weatherClient.getWeatherFromCache(
            this._correlationId, this.sendResult(req, res)
        );
    }

    public getWeather(req: any, res: any): void {
        this._weatherClient.getWeather(
            this._correlationId, this.sendResult(req, res)
        );
    }
}