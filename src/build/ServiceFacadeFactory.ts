import { CompositeFactory } from 'pip-services3-components-node';

import { LoggingServiceFactory } from 'pip-services-logging-node';

import { AdsServiceFactory } from 'ad-board-services-ads-node';
import { DevicegroupsServiceFactory } from 'ad-board-services-devicegroups-node';
import { DevicesServiceFactory } from 'ad-board-services-devices-node';
import { BlobsServiceFactory } from 'pip-services-blobs-node/obj/src/build/BlobsServiceFactory';

export class ServiceFacadeFactory extends CompositeFactory {
    public constructor() {
        super();

        this.add(new LoggingServiceFactory());
        this.add(new AdsServiceFactory());
        this.add(new DevicegroupsServiceFactory());
        this.add(new DevicesServiceFactory());
        this.add(new BlobsServiceFactory());
    }
}
