import { CompositeFactory } from 'pip-services3-components-node';
import { LoggingClientFactory } from 'pip-clients-logging-node';
import { AdsClientFactory } from 'ad-board-clients-ads-node';
import { DevicegroupsClientFactory } from 'ad-board-clients-devicegroups-node';
import { DevicesClientFactory } from 'ad-board-clients-devices-node';
import { BlobsClientFactory } from 'pip-clients-blobs-node';
import { DefaultYandexWeatherFactory } from 'ad-board-clients-yandexweather-node';

export class ClientFacadeFactory extends CompositeFactory {
    public constructor() {
        super();

        this.add(new LoggingClientFactory());
        this.add(new AdsClientFactory());
        this.add(new DevicegroupsClientFactory());
        this.add(new DevicesClientFactory());
        this.add(new BlobsClientFactory());
        this.add(new DefaultYandexWeatherFactory());
    }
}
 