import { IReferences } from 'pip-services3-commons-node';
import { ConfigParams } from 'pip-services3-commons-node';
import { RestService } from 'pip-services3-rpc-node';
import { AboutOperations } from 'pip-services3-rpc-node';

import { AuthorizerV1 } from './AuthorizerV1';

import { LoggingOperationsV1 } from '../../operations/version1/LoggingOperationsV1';

import { DevicegroupsOperationsV1 } from '../../operations/version1/DevicegroupsOperationsV1';
import { DevicesOperationsV1 } from '../../operations/version1/DevicesOperationsV1';
import { AdsOperationsV1 } from '../../operations/version1/AdsOperationsV1';
import { BoardsOperationsV1 } from '../../operations/version1/BoardsOperationsV1';
import { BlobsOperationsV1 } from '../../operations/version1/BlobsOperationsV1';
import { WeatherOperationsV1 } from '../../operations/version1/WeatherOperationsV1';


export class FacadeServiceV1 extends RestService {
    private _aboutOperations = new AboutOperations();
    private _loggingOperations = new LoggingOperationsV1();


    private _adsOperationsV1 = new AdsOperationsV1();
    private _devicegroupsOperationsV1 = new DevicegroupsOperationsV1();
    private _devicesOperationsV1 = new DevicesOperationsV1();
    private _boardsOperationsV1 = new BoardsOperationsV1();
    private _blobsOperationsV1 = new BlobsOperationsV1();
    private _weatherOperationsV1 = new WeatherOperationsV1();


    public constructor() {
        super();
        this._baseRoute = "api/v1"
    }

    public configure(config: ConfigParams): void {
        super.configure(config);

        this._aboutOperations.configure(config);
        this._loggingOperations.configure(config);

        this._adsOperationsV1.configure(config);
        this._devicegroupsOperationsV1.configure(config);
        this._devicesOperationsV1.configure(config);
        this._boardsOperationsV1.configure(config);
        this._blobsOperationsV1.configure(config);
        this._weatherOperationsV1.configure(config);
    }

    public setReferences(references: IReferences): void {
        super.setReferences(references);

        this._aboutOperations.setReferences(references);
        this._loggingOperations.setReferences(references);

        this._adsOperationsV1.setReferences(references);
        this._devicegroupsOperationsV1.setReferences(references);
        this._devicesOperationsV1.setReferences(references);
        this._boardsOperationsV1.setReferences(references);
        this._blobsOperationsV1.setReferences(references);
        this._weatherOperationsV1.setReferences(references);
    }

    public register(): void {
        let auth = new AuthorizerV1();

        // Restore session middleware
        // this.registerInterceptor('',
        //     (req, res, next) => { this._sessionsOperations.loadSession(req, res, next); });

        this.registerAdsOperations(auth);
        this.registerDevicegroupsOperations(auth);
        this.registerDevicesOperations(auth);
        this.registerBoardOperations(auth);
        this.registerBlobsOperations(auth);
        this.registerWeatherOperations(auth);
    }


    private registerAdsOperations(auth: AuthorizerV1): void {

        // Admin tools ads Routes
        this.registerRouteWithAuth('get', '/admin/ads', null, auth.anybody(),
            (req, res) => { this._adsOperationsV1.getAds(req, res); });
        this.registerRouteWithAuth('get', '/admin/ads/:ad_id', null, auth.anybody(),
            (req, res) => { this._adsOperationsV1.getAd(req, res); });
        this.registerRouteWithAuth('post', '/admin/ads', null, auth.anybody(),
            (req, res) => { this._adsOperationsV1.createAd(req, res); });
        this.registerRouteWithAuth('put', '/admin/ads/:ad_id', null, auth.anybody(),
            (req, res) => { this._adsOperationsV1.updateAd(req, res); });
        this.registerRouteWithAuth('del', '/admin/ads/:ad_id', null, auth.anybody(),
            (req, res) => { this._adsOperationsV1.deleteAd(req, res); });
    }

    private registerDevicegroupsOperations(auth: AuthorizerV1): void {

        // Admin tools device groups Routes
        this.registerRouteWithAuth('get', '/admin/devs_groups', null, auth.anybody(),
            (req, res) => { this._devicegroupsOperationsV1.getDeviceGroups(req, res); });
        this.registerRouteWithAuth('get', '/admin/devs_groups/:group_id', null, auth.anybody(),
            (req, res) => { this._devicegroupsOperationsV1.getDeviceGroup(req, res); });
        this.registerRouteWithAuth('post', '/admin/devs_groups', null, auth.anybody(),
            (req, res) => { this._devicegroupsOperationsV1.createDeviceGroup(req, res); });
        this.registerRouteWithAuth('put', '/admin/devs_groups/:group_id', null, auth.anybody(),
            (req, res) => { this._devicegroupsOperationsV1.updateDeviceGroup(req, res); });
        this.registerRouteWithAuth('del', '/admin/devs_groups/:group_id', null, auth.anybody(),
            (req, res) => { this._devicegroupsOperationsV1.deleteDeviceGroup(req, res); });
    }

    private registerDevicesOperations(auth: AuthorizerV1): void {

        // Admin tools device Routes
        this.registerRouteWithAuth('get', '/admin/devs', null, auth.anybody(),
            (req, res) => { this._devicesOperationsV1.getDevices(req, res); });
        this.registerRouteWithAuth('get', '/admin/devs/:dev_id', null, auth.anybody(),
            (req, res) => { this._devicesOperationsV1.getDevice(req, res); });
        this.registerRouteWithAuth('get', '/admin/devs/:dev_id/id', null, auth.anybody(),
            (req, res) => { this._devicesOperationsV1.getDeviceCode(req, res); });
        this.registerRouteWithAuth('post', '/admin/devs', null, auth.anybody(),
            (req, res) => { this._devicesOperationsV1.createDevice(req, res); });
        this.registerRouteWithAuth('put', '/admin/devs/:dev_id', null, auth.anybody(),
            (req, res) => { this._devicesOperationsV1.updateDevice(req, res); });
        this.registerRouteWithAuth('del', '/admin/devs/:dev_id', null, auth.anybody(),
            (req, res) => { this._devicesOperationsV1.deleteDevice(req, res); });
    }

    private registerBoardOperations(auth: AuthorizerV1): void {
        this.registerRouteWithAuth('get', '/board/ads/:dev_id', null, auth.anybody(),
            (req, res) => { this._boardsOperationsV1.getAds(req, res); });
    }

    private registerBlobsOperations(auth: AuthorizerV1): void {
        // Blobs routes
        this.registerRouteWithAuth('get', '/content/blobs', null, auth.anybody(),
            (req, res) => { this._blobsOperationsV1.getBlobs(req, res); });
        this.registerRouteWithAuth('get', '/content/blobs/:blob_id/info', null, auth.anybody(),
            (req, res) => { this._blobsOperationsV1.getBlobInfo(req, res); });
        this.registerRouteWithAuth('get', '/content/blobs/:blob_id', null, auth.anybody(),
            (req, res) => { this._blobsOperationsV1.getBlob(req, res); });
        this.registerRouteWithAuth('post', '/content/blobs', null, auth.anybody(),
            (req, res) => { this._blobsOperationsV1.setBlob(req, res); });
        this.registerRouteWithAuth('post', '/content/blobs/:blob_id', null, auth.anybody(),
            (req, res) => { this._blobsOperationsV1.setBlob(req, res); });
        // this.registerRouteWithAuth('post', '/content/blobs/url', null, auth.anybody(),
        //     (req, res) => { this._blobsOperationsV1.loadBlobFromUrl(req, res); });
        this.registerRouteWithAuth('put', '/content/blobs/:blob_id/info', null, auth.anybody(),
            (req, res) => { this._blobsOperationsV1.updateBlobInfo(req, res); });
        this.registerRouteWithAuth('put', '/content/blobs/:blob_id', null, auth.anybody(),
            (req, res) => { this._blobsOperationsV1.setBlob(req, res); });
        // this.registerRouteWithAuth('put', '/content/blobs/:blob_id/url', null, auth.anybody(),
        //     (req, res) => { this._blobsOperationsV1.loadBlobFromUrl(req, res); });
        this.registerRouteWithAuth('del', '/content/blobs/:blob_id', null, auth.anybody(),
            (req, res) => { this._blobsOperationsV1.deleteBlob(req, res); });
    }

    private registerWeatherOperations(auth: AuthorizerV1): void {
        this.registerRouteWithAuth('get', '/weather/cache', null, auth.anybody(),
            (req, res) => { this._weatherOperationsV1.getWeatherFromCache(req, res); });

        this.registerRouteWithAuth('get', '/weather/now', null, auth.anybody(),
            (req, res) => { this._weatherOperationsV1.getWeather(req, res); });

    }

}
