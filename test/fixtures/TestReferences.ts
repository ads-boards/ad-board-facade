const _ = require('lodash');

import { ConfigParams } from 'pip-services3-commons-node';
import { Descriptor } from 'pip-services3-commons-node';

import { CompositeFactory } from 'pip-services3-components-node';
import { ManagedReferences } from 'pip-services3-container-node';


import { TestUsers } from './TestUsers';
import { ClientFacadeFactory } from '../../src/build/ClientFacadeFactory';
import { ServiceFacadeFactory } from '../../src/build/ServiceFacadeFactory';
import { HttpEndpoint } from 'pip-services3-rpc-node';
import { DefaultRpcFactory } from 'pip-services3-rpc-node';
import { FacadeServiceV1 } from '../../src/services/version1/FacadeServiceV1';
import { YandexWeatherClientV1 } from 'ad-board-clients-yandexweather-node';

export class TestReferences extends ManagedReferences {
    private _factory = new CompositeFactory();

    public constructor() {
        super();

        this.setupFactories();
        this.appendDependencies();
        this.configureService();
        //this.createUsersAndSessions();
    }

    private setupFactories() {
        this._factory.add(new ClientFacadeFactory());
        this._factory.add(new ServiceFacadeFactory());
        this._factory.add(new DefaultRpcFactory());
    }

    public append(descriptor: Descriptor): void {
        let component = this._factory.create(descriptor);
        this.put(descriptor, component);
    }

    private appendDependencies() {
        // Add factories
        this.put(null, this._factory);

        // Add service
        this.put(null, new FacadeServiceV1());

        // Add infrastructure services
        this.append(new Descriptor('pip-services-logging', 'persistence-messages', 'memory', 'default', '*'));
        this.append(new Descriptor('pip-services-logging', 'persistence-errors', 'memory', 'default', '*'));
        this.append(new Descriptor('pip-services-logging', 'controller', 'default', 'default', '*'));
        this.append(new Descriptor('pip-services-logging', 'client', 'direct', 'default', '*'));

        // Add admin tool ads
        this.append(new Descriptor('ad-board-ads', 'persistence', 'memory', 'default', '*'));
        this.append(new Descriptor('ad-board-ads', 'controller', 'default', 'default', '*'));
        this.append(new Descriptor('ad-board-ads', 'client', 'direct', 'default', '*'));

        // Add admin tool devices
        this.append(new Descriptor('ad-board-devices', 'persistence', 'memory', 'default', '*'));
        this.append(new Descriptor('ad-board-devices', 'controller', 'default', 'default', '*'));
        this.append(new Descriptor('ad-board-devices', 'client', 'direct', 'default', '*'));

        // Add admin tool device groups
        this.append(new Descriptor('ad-board-devicegroups', 'persistence', 'memory', 'default', '*'));
        this.append(new Descriptor('ad-board-devicegroups', 'controller', 'default', 'default', '*'));
        this.append(new Descriptor('ad-board-devicegroups', 'client', 'direct', 'default', '*'));

        // Add blobs references
        this.append(new Descriptor('pip-services-blobs', 'persistence', 'memory', 'default', '*'));
        this.append(new Descriptor('pip-services-blobs', 'controller', 'default', 'default', '*'));
        this.append(new Descriptor('pip-services-blobs', 'client', 'direct', 'default', '*'));

        // Yandex weather
        this.append(new Descriptor('ad-board-yandexweather', 'client', 'direct', 'default', '*'));
    }

    private configureService(): void {
        // Configure Facade service
        let serviceWether = this.getOneRequired<YandexWeatherClientV1>(
            new Descriptor('ad-board-yandexweather', 'client', 'direct', 'default', '*')
        );
        serviceWether.configure(ConfigParams.fromTuples(
            'credential.access_key', " ",
            'options.update_interval', 1000, // 1 sec
            'options.lat', '55.75396',
            'options.lon', '37.620393',
        ));

        // Configure Facade service
        let service = this.getOneRequired<HttpEndpoint>(
            new Descriptor('pip-services', 'endpoint', 'http', 'default', '*')
        );
        service.configure(ConfigParams.fromTuples(
            'root_path', '', //'/api/v1',
            'connection.protocol', 'http',
            'connection.host', '0.0.0.0',
            'connection.port', 3000
        ));
    }
}