let _ = require('lodash');
let async = require('async');
let assert = require('chai').assert;

import { TestUsers } from '../../fixtures/TestUsers';
import { TestReferences } from '../../fixtures/TestReferences';
import { TestRestClient } from '../../fixtures/TestRestClient';
import { AdSizeV1, AdV1 } from 'ad-board-clients-ads-node';
import { DevicegroupV1 } from 'ad-board-clients-devicegroups-node';
import { DeviceV1 } from 'ad-board-clients-devices-node';

let now = new Date();

let AD1: AdV1 = {
    id: '1',
    publish_group_ids: ['g1', 'g2'],
    title: 'Title 1',
    text: 'Ad text 1',
    img: '',
    text_color: 'black',
    text_size: 14,
    background_color: 'white',
    critical: false,
    deleted: false,
    disabled: false,
    size: AdSizeV1.Small,
    create_time: now,
    end_time: new Date(now.getTime() + 30000)
};
let AD2: AdV1 = {
    id: '2',
    publish_group_ids: ['g2', 'g3'],
    title: 'Title 2',
    text: 'Ad text 2',
    img: '',
    text_color: 'black',
    text_size: 14,
    background_color: 'white',
    critical: false,
    deleted: false,
    disabled: false,
    size: AdSizeV1.Small,
    create_time: now,
    end_time: new Date(now.getTime() + 30000)
};

let DEVICE1: DeviceV1 = {
    id: 'dev1',
    request_code: 'id123',
    description: 'Device 1',
    last_connection: now
};

let DEVICE2: DeviceV1 = {
    id: 'dev2',
    request_code: 'id321',
    description: 'Device 2',
    last_connection: now
};

let GROUP1: DevicegroupV1 = {
    id: 'g1',
    device_ids: ['dev1', 'dev2'],
    description: 'Group 1',

};
let GROUP2: DevicegroupV1 = {
    id: 'g2',
    device_ids: ['dev3', 'dev4'],
    description: 'Group 2',

};

suite('BoardsOperationsV1', () => {
    let references: TestReferences;
    let rest: TestRestClient;

    setup((done) => {
        rest = new TestRestClient();
        references = new TestReferences();
        references.open(null, done);
    });

    teardown((done) => {
        references.close(null, done);
    });

    test('should perform boards operations', (done) => {
        let ad1;
        let dev1;
        let dev_grp1;

        async.series([

            // Create one dev_grp
            (callback) => {
                rest.postAsUser(
                    TestUsers.AdminUserSessionId,
                    '/api/v1/admin/devs_groups',
                    GROUP1,
                    (err, req, res, dev_grp) => {
                        assert.isNull(err);

                        assert.isObject(dev_grp);
                        assert.equal(dev_grp.description, GROUP1.description);

                        dev_grp1 = dev_grp;

                        callback();
                    }
                );
            },
            // Create another dev_grp
            (callback) => {
                rest.postAsUser(
                    TestUsers.AdminUserSessionId,
                    '/api/v1/admin/devs_groups',
                    GROUP2,
                    (err, req, res, dev_grp) => {
                        assert.isNull(err);

                        assert.isObject(dev_grp);
                        assert.equal(dev_grp.description, GROUP2.description);

                        callback();
                    }
                );
            },
            // Get all dev_grps
            (callback) => {
                rest.getAsUser(
                    TestUsers.User1SessionId,
                    '/api/v1/admin/devs_groups',
                    (err, req, res, page) => {
                        assert.isNull(err);

                        assert.isObject(page);
                        assert.lengthOf(page.data, 2);

                        callback();
                    }
                );
            },

            // Create one dev
            (callback) => {
                rest.postAsUser(
                    TestUsers.AdminUserSessionId,
                    '/api/v1/admin/devs',
                    DEVICE1,
                    (err, req, res, dev) => {
                        assert.isNull(err);

                        assert.isObject(dev);
                        assert.equal(dev.request_code, DEVICE1.request_code);

                        dev1 = dev;

                        callback();
                    }
                );
            },
            // Create another dev
            (callback) => {
                rest.postAsUser(
                    TestUsers.AdminUserSessionId,
                    '/api/v1/admin/devs',
                    DEVICE2,
                    (err, req, res, dev) => {
                        assert.isNull(err);

                        assert.isObject(dev);
                        assert.equal(dev.request_code, DEVICE2.request_code);

                        callback();
                    }
                );
            },
            // Get all devs
            (callback) => {
                rest.getAsUser(
                    TestUsers.User1SessionId,
                    '/api/v1/admin/devs',
                    (err, req, res, page) => {
                        assert.isNull(err);

                        assert.isObject(page);
                        assert.lengthOf(page.data, 2);

                        callback();
                    }
                );
            },

            // Create one ad
            (callback) => {
                rest.postAsUser(
                    TestUsers.AdminUserSessionId,
                    '/api/v1/admin/ads',
                    AD1,
                    (err, req, res, ad) => {
                        assert.isNull(err);

                        assert.isObject(ad);
                        assert.equal(ad.title, AD1.title);

                        ad1 = ad;

                        callback();
                    }
                );
            },
            // Create another ad
            (callback) => {
                rest.postAsUser(
                    TestUsers.AdminUserSessionId,
                    '/api/v1/admin/ads',
                    AD2,
                    (err, req, res, ad) => {
                        assert.isNull(err);

                        assert.isObject(ad);
                        assert.equal(ad.title, AD2.title);

                        callback();
                    }
                );
            },
            // Get all ads
            (callback) => {
                rest.getAsUser(
                    TestUsers.User1SessionId,
                    '/api/v1/admin/ads',
                    (err, req, res, page) => {
                        assert.isNull(err);

                        assert.isObject(page);
                        assert.lengthOf(page.data, 2);

                        callback();
                    }
                );
            },

            // Get all ads for device 1
            (callback) => {
                rest.getAsUser(
                    TestUsers.User1SessionId,
                    '/api/v1/board/ads/'+DEVICE1.request_code,
                    (err, req, res, page) => {
                        assert.isNull(err);

                        assert.isObject(page);
                        assert.lengthOf(page.data, 1);

                        callback();
                    }
                );
            },


        ], done);
    });

});