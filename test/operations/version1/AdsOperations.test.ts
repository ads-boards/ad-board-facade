let _ = require('lodash');
let async = require('async');
let assert = require('chai').assert;

import { TestUsers } from '../../fixtures/TestUsers';
import { TestReferences } from '../../fixtures/TestReferences';
import { TestRestClient } from '../../fixtures/TestRestClient';
import { AdSizeV1, AdV1 } from 'ad-board-clients-ads-node';

let now = new Date();

let AD1: AdV1 = {
    id: '1',
    publish_group_ids: ['1', '2'], 
    title: 'Title 1',
    text: 'Ad text 1',
    img: '', 
    text_color:'black',
    text_size: 14,
    background_color: 'white',
    critical: false, 
    deleted: false,
    disabled: false,
    size: AdSizeV1.Small,
    create_time: now,
    end_time: new Date(now.getTime() + 3000)
};
let AD2: AdV1 = {
   id: '2',
   publish_group_ids: ['1', '3'], 
   title: 'Title 2',
   text: 'Ad text 2',
   img: '', 
   text_color:'black',
   text_size: 14,
   background_color: 'white',
   critical: false, 
   deleted: false,
   disabled: false,
   size: AdSizeV1.Small,
   create_time: now,
   end_time: new Date(now.getTime() + 3000)
};

suite('AdminToolsAdsOperationsV1', () => {
    let references: TestReferences;
    let rest: TestRestClient;

    setup((done) => {
        rest = new TestRestClient();
        references = new TestReferences();
        references.open(null, done);
    });

    teardown((done) => {
        references.close(null, done);
    });

    test('should perform ads operations', (done) => {
        let ad1;

        async.series([
            // Create one ad
            (callback) => {
                rest.postAsUser(
                    TestUsers.AdminUserSessionId,
                    '/api/v1/admin/ads',
                    AD1,
                    (err, req, res, ad) => {
                        assert.isNull(err);

                        assert.isObject(ad);
                        assert.equal(ad.title, AD1.title);

                        ad1 = ad;

                        callback();
                    }
                );
            },
            // Create another ad
            (callback) => {
                rest.postAsUser(
                    TestUsers.AdminUserSessionId,
                    '/api/v1/admin/ads',
                    AD2,
                    (err, req, res, ad) => {
                        assert.isNull(err);

                        assert.isObject(ad);
                        assert.equal(ad.title, AD2.title);

                        callback();
                    }
                );
            },
            // Get all ads
            (callback) => {
                rest.getAsUser(
                    TestUsers.User1SessionId,
                    '/api/v1/admin/ads',
                    (err, req, res, page) => {
                        assert.isNull(err);

                        assert.isObject(page);
                        assert.lengthOf(page.data, 2);

                        callback();
                    }
                );
            },
            // Update the ad
            (callback) => {
                ad1.text = 'Updated Content 1';

                rest.putAsUser(
                    TestUsers.AdminUserSessionId,
                    '/api/v1/admin/ads/' + ad1.id,
                    ad1,
                    (err, req, res, ad) => {
                        assert.isNull(err);

                        assert.isObject(ad);
                        assert.equal(ad.title, AD1.title);
                        assert.equal(ad.text, 'Updated Content 1');

                        ad1 = ad;

                        callback();
                    }
                );
            },
            // Delete ad
            (callback) => {
                rest.delAsUser(
                    TestUsers.AdminUserSessionId,
                    '/api/v1/admin/ads/' + ad1.id,
                    (err, req, res, result) => {
                        assert.isNull(err);

                        //assert.isNull(result);

                        callback();
                    }
                );
            },
            // Try to get delete ad
            (callback) => {
                rest.getAsUser(
                    TestUsers.AdminUserSessionId,
                    '/api/v1/admin/ads/' + ad1.id,
                    (err, req, res, result) => {
                        assert.isNull(err);

                        //assert.isNull(result);

                        callback();
                    }
                );
            }
        ], done);
    });

});