let _ = require('lodash');
let async = require('async');
let assert = require('chai').assert;

import { TestUsers } from '../../fixtures/TestUsers';
import { TestReferences } from '../../fixtures/TestReferences';
import { TestRestClient } from '../../fixtures/TestRestClient';

suite('YandexWeatherOperationsV1', () => {
    let references: TestReferences;
    let rest: TestRestClient;

    setup((done) => {
        rest = new TestRestClient();
        references = new TestReferences();
        references.open(null, done);
    });

    teardown((done) => {
        references.close(null, done);
    });

    test('should perform yandex weather cache operations', (done) => {
        rest.getAsUser(
            TestUsers.User1SessionId,
            '/api/v1//weather/cache',
            (err, req, res, weather) => {
                assert.isNotNull(err);
               // assert.isObject(weather);
                done();
            }
        );
    });

    test('should perform yandex weather operations', (done) => {
        rest.getAsUser(
            TestUsers.User1SessionId,
            '/api/v1/weather/now',
            (err, req, res, weather) => {
                assert.isNotNull(err);
              //  assert.isObject(weather);
                done();
            }
        );
    });

});