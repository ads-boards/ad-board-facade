let _ = require('lodash');
let async = require('async');
let assert = require('chai').assert;

import { TestUsers } from '../../fixtures/TestUsers';
import { TestReferences } from '../../fixtures/TestReferences';
import { TestRestClient } from '../../fixtures/TestRestClient';
import { DeviceV1 } from 'ad-board-clients-devices-node';


let DEVICE1: DeviceV1 = {
    id: '1',
    request_code: 'id123',
    description: 'Device 1',
    last_connection: new Date()
};
let DEVICE2: DeviceV1 = {
    id: '2',
    request_code: 'id321',
    description: 'Device 2',
    last_connection: new Date()
};

suite('DevicesOperationsV1', () => {
    let references: TestReferences;
    let rest: TestRestClient;

    setup((done) => {
        rest = new TestRestClient();
        references = new TestReferences();
        references.open(null, done);
    });

    teardown((done) => {
        references.close(null, done);
    });

    test('should perform devices operations', (done) => {
        let dev1;

        async.series([
            // Create one dev
            (callback) => {
                rest.postAsUser(
                    TestUsers.AdminUserSessionId,
                    '/api/v1/admin/devs',
                    DEVICE1,
                    (err, req, res, dev) => {
                        assert.isNull(err);

                        assert.isObject(dev);
                        assert.equal(dev.request_code, DEVICE1.request_code);

                        dev1 = dev;

                        callback();
                    }
                );
            },
            // Create another dev
            (callback) => {
                rest.postAsUser(
                    TestUsers.AdminUserSessionId,
                    '/api/v1/admin/devs',
                    DEVICE2,
                    (err, req, res, dev) => {
                        assert.isNull(err);

                        assert.isObject(dev);
                        assert.equal(dev.request_code, DEVICE2.request_code);

                        callback();
                    }
                );
            },
            // Get all devs
            (callback) => {
                rest.getAsUser(
                    TestUsers.User1SessionId,
                    '/api/v1/admin/devs',
                    (err, req, res, page) => {
                        assert.isNull(err);

                        assert.isObject(page);
                        assert.lengthOf(page.data, 2);

                        callback();
                    }
                );
            },
            // Generate request code
            (callback) => {
                rest.getAsUser(
                    TestUsers.AdminUserSessionId,'/api/v1/admin/devs/' + DEVICE1.id+'/id',
                    (err, req, res, result) => {
                        assert.isNull(err);
                        assert.isNotNull(result.code);
                        callback();
                    }
                );
            },
            // Update the dev
            (callback) => {
                dev1.description = 'Updated Content 1';

                rest.putAsUser(
                    TestUsers.AdminUserSessionId,
                    '/api/v1/admin/devs/' + dev1.id,
                    dev1,
                    (err, req, res, dev) => {
                        assert.isNull(err);

                        assert.isObject(dev);
                        assert.equal(dev.request_code, DEVICE1.request_code);
                        assert.equal(dev.description, 'Updated Content 1');

                        dev1 = dev;

                        callback();
                    }
                );
            },
            // Delete dev
            (callback) => {
                rest.delAsUser(
                    TestUsers.AdminUserSessionId,
                    '/api/v1/admin/devs/' + dev1.id,
                    (err, req, res, result) => {
                        assert.isNull(err);

                        //assert.isNull(result);

                        callback();
                    }
                );
            },
            // Try to get delete dev
            (callback) => {
                rest.getAsUser(
                    TestUsers.AdminUserSessionId,
                    '/api/v1/admin/devs/' + dev1.id,
                    (err, req, res, result) => {
                        assert.isNull(err);

                        //assert.isNull(result);

                        callback();
                    }
                );
            }
        ], done);
    });

});