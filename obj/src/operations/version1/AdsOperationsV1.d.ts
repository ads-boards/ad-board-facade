import { IReferences } from 'pip-services3-commons-node';
import { RestOperations } from 'pip-services3-rpc-node';
export declare class AdsOperationsV1 extends RestOperations {
    private _adsClient;
    private _correlationId;
    constructor();
    setReferences(references: IReferences): void;
    getAds(req: any, res: any): void;
    getAd(req: any, res: any): void;
    createAd(req: any, res: any): void;
    updateAd(req: any, res: any): void;
    deleteAd(req: any, res: any): void;
}
