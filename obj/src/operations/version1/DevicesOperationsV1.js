"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicesOperationsV1 = void 0;
let _ = require('lodash');
let async = require('async');
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_rpc_node_1 = require("pip-services3-rpc-node");
class DevicesOperationsV1 extends pip_services3_rpc_node_1.RestOperations {
    constructor() {
        super();
        this._correlationId = "devices-operations";
        this._dependencyResolver.put('devices-client', new pip_services3_commons_node_1.Descriptor('ad-board-devices', 'client', '*', '*', '1.0'));
    }
    setReferences(references) {
        super.setReferences(references);
        this._devicesClient = this._dependencyResolver.getOneRequired('devices-client');
    }
    getDevices(req, res) {
        let filter = this.getFilterParams(req);
        let paging = this.getPagingParams(req);
        this._devicesClient.getDevices(this._correlationId, filter, paging, this.sendResult(req, res));
    }
    getDeviceCode(req, res) {
        let devId = req.route.params.dev_id;
        this._devicesClient.generateRequstCode(this._correlationId, devId, (err, code) => {
            this.sendResult(req, res)(err, { code: code });
        });
    }
    getDevice(req, res) {
        let devId = req.route.params.dev_id;
        this._devicesClient.getDeviceById(this._correlationId, devId, this.sendResult(req, res));
    }
    createDevice(req, res) {
        let dev = req.body || {};
        dev.last_connection = new Date(dev.last_connection);
        this._devicesClient.createDevice(this._correlationId, dev, this.sendResult(req, res));
    }
    updateDevice(req, res) {
        let devId = req.route.params.dev_id;
        let dev = req.body || {};
        dev.id = devId;
        dev.last_connection = new Date(dev.last_connection) || new Date();
        this._devicesClient.updateDevice(this._correlationId, dev, this.sendResult(req, res));
    }
    deleteDevice(req, res) {
        let devId = req.route.params.dev_id;
        this._devicesClient.deleteDeviceById(this._correlationId, devId, this.sendResult(req, res));
    }
}
exports.DevicesOperationsV1 = DevicesOperationsV1;
//# sourceMappingURL=DevicesOperationsV1.js.map