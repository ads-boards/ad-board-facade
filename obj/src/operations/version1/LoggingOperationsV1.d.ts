import { IReferences } from 'pip-services3-commons-node';
import { RestOperations } from 'pip-services3-rpc-node';
export declare class LoggingOperationsV1 extends RestOperations {
    private _loggingClient;
    constructor();
    setReferences(references: IReferences): void;
    getMessages(req: any, res: any): void;
    getErrors(req: any, res: any): void;
    private messageToText;
    private messagesToText;
    getMessagesAsText(req: any, res: any): void;
    getErrorsAsText(req: any, res: any): void;
    writeMessage(req: any, res: any): void;
    clearMessages(req: any, res: any): void;
}
