import { IReferences } from 'pip-services3-commons-node';
import { RestOperations } from 'pip-services3-rpc-node';
export declare class DevicegroupsOperationsV1 extends RestOperations {
    private _devicegroupsClient;
    private _correlationId;
    constructor();
    setReferences(references: IReferences): void;
    getDeviceGroups(req: any, res: any): void;
    getDeviceGroup(req: any, res: any): void;
    createDeviceGroup(req: any, res: any): void;
    updateDeviceGroup(req: any, res: any): void;
    deleteDeviceGroup(req: any, res: any): void;
}
