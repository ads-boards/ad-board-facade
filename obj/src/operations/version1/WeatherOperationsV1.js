"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WeatherOperationsV1 = void 0;
let _ = require('lodash');
let async = require('async');
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_rpc_node_1 = require("pip-services3-rpc-node");
class WeatherOperationsV1 extends pip_services3_rpc_node_1.RestOperations {
    constructor() {
        super();
        this._correlationId = "weather-operations";
        this._dependencyResolver.put('weather-client', new pip_services3_commons_node_1.Descriptor('ad-board-yandexweather', 'client', '*', '*', '1.0'));
    }
    setReferences(references) {
        super.setReferences(references);
        this._weatherClient = this._dependencyResolver.getOneRequired('weather-client');
    }
    getWeatherFromCache(req, res) {
        this._weatherClient.getWeatherFromCache(this._correlationId, this.sendResult(req, res));
    }
    getWeather(req, res) {
        this._weatherClient.getWeather(this._correlationId, this.sendResult(req, res));
    }
}
exports.WeatherOperationsV1 = WeatherOperationsV1;
//# sourceMappingURL=WeatherOperationsV1.js.map