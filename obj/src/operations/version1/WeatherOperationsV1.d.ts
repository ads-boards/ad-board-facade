import { IReferences } from 'pip-services3-commons-node';
import { RestOperations } from 'pip-services3-rpc-node';
export declare class WeatherOperationsV1 extends RestOperations {
    private _weatherClient;
    private _correlationId;
    constructor();
    setReferences(references: IReferences): void;
    getWeatherFromCache(req: any, res: any): void;
    getWeather(req: any, res: any): void;
}
