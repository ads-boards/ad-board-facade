"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicegroupsOperationsV1 = void 0;
let _ = require('lodash');
let async = require('async');
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_rpc_node_1 = require("pip-services3-rpc-node");
class DevicegroupsOperationsV1 extends pip_services3_rpc_node_1.RestOperations {
    constructor() {
        super();
        this._correlationId = "devicegroups-operations";
        this._dependencyResolver.put('device_groups-client', new pip_services3_commons_node_1.Descriptor('ad-board-devicegroups', 'client', '*', '*', '1.0'));
    }
    setReferences(references) {
        super.setReferences(references);
        this._devicegroupsClient = this._dependencyResolver.getOneRequired('device_groups-client');
    }
    getDeviceGroups(req, res) {
        let filter = this.getFilterParams(req);
        let paging = this.getPagingParams(req);
        this._devicegroupsClient.getDevicegroups(this._correlationId, filter, paging, this.sendResult(req, res));
    }
    getDeviceGroup(req, res) {
        let groupId = req.route.params.group_id;
        this._devicegroupsClient.getDevicegroupById(this._correlationId, groupId, this.sendResult(req, res));
    }
    createDeviceGroup(req, res) {
        let devgrp = req.body || {};
        this._devicegroupsClient.createDevicegroup(this._correlationId, devgrp, this.sendResult(req, res));
    }
    updateDeviceGroup(req, res) {
        let groupId = req.route.params.group_id;
        let devgrp = req.body || {};
        devgrp.id = groupId;
        this._devicegroupsClient.updateDevicegroup(this._correlationId, devgrp, this.sendResult(req, res));
    }
    deleteDeviceGroup(req, res) {
        let groupId = req.route.params.group_id;
        this._devicegroupsClient.deleteDevicegroupById(this._correlationId, groupId, this.sendResult(req, res));
    }
}
exports.DevicegroupsOperationsV1 = DevicegroupsOperationsV1;
//# sourceMappingURL=DevicegroupsOperationsV1.js.map