"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdsOperationsV1 = void 0;
let _ = require('lodash');
let async = require('async');
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_rpc_node_1 = require("pip-services3-rpc-node");
class AdsOperationsV1 extends pip_services3_rpc_node_1.RestOperations {
    constructor() {
        super();
        this._correlationId = "ads-operations";
        this._dependencyResolver.put('ads-client', new pip_services3_commons_node_1.Descriptor('ad-board-ads', 'client', '*', '*', '1.0'));
    }
    setReferences(references) {
        super.setReferences(references);
        this._adsClient = this._dependencyResolver.getOneRequired('ads-client');
    }
    getAds(req, res) {
        let filter = this.getFilterParams(req);
        let paging = this.getPagingParams(req);
        this._adsClient.getAds(this._correlationId, filter, paging, this.sendResult(req, res));
    }
    getAd(req, res) {
        let adId = req.route.params.ad_id;
        this._adsClient.getAdById(this._correlationId, adId, this.sendResult(req, res));
    }
    createAd(req, res) {
        let ad = req.body || {};
        ad.create_time = new Date(ad.create_time) || new Date();
        ad.end_time = ad.end_time ? new Date(ad.end_time) : ad.end_time;
        this._adsClient.createAd(this._correlationId, ad, this.sendResult(req, res));
    }
    updateAd(req, res) {
        let adId = req.route.params.ad_id;
        let ad = req.body || {};
        ad.id = adId;
        ad.create_time = new Date(ad.create_time);
        ad.end_time = ad.end_time ? new Date(ad.end_time) : ad.end_time;
        this._adsClient.updateAd(this._correlationId, ad, this.sendResult(req, res));
    }
    deleteAd(req, res) {
        let adId = req.route.params.ad_id;
        this._adsClient.deleteAdById(this._correlationId, adId, this.sendResult(req, res));
    }
}
exports.AdsOperationsV1 = AdsOperationsV1;
//# sourceMappingURL=AdsOperationsV1.js.map