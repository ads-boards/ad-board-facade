import { IReferences } from 'pip-services3-commons-node';
import { RestOperations } from 'pip-services3-rpc-node';
export declare class BlobsOperationsV1 extends RestOperations {
    private _blobsClient;
    constructor();
    setReferences(references: IReferences): void;
    getBlobs(req: any, res: any): void;
    getBlobInfo(req: any, res: any): void;
    getBlob(req: any, res: any): void;
    setBlob(req: any, res: any): void;
    loadBlobFromUrl(req: any, res: any): void;
    updateBlobInfo(req: any, res: any): void;
    deleteBlob(req: any, res: any): void;
}
