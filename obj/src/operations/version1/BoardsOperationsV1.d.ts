import { IReferences } from 'pip-services3-commons-node';
import { RestOperations } from 'pip-services3-rpc-node';
export declare class BoardsOperationsV1 extends RestOperations {
    private _adsClient;
    private _devicegroupsClient;
    private _devicesClient;
    private _correlationId;
    constructor();
    setReferences(references: IReferences): void;
    getAds(req: any, res: any): void;
}
