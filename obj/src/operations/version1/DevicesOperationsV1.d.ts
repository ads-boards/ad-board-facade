import { IReferences } from 'pip-services3-commons-node';
import { RestOperations } from 'pip-services3-rpc-node';
export declare class DevicesOperationsV1 extends RestOperations {
    private _devicesClient;
    private _correlationId;
    constructor();
    setReferences(references: IReferences): void;
    getDevices(req: any, res: any): void;
    getDeviceCode(req: any, res: any): void;
    getDevice(req: any, res: any): void;
    createDevice(req: any, res: any): void;
    updateDevice(req: any, res: any): void;
    deleteDevice(req: any, res: any): void;
}
