"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BoardsOperationsV1 = void 0;
let _ = require('lodash');
let async = require('async');
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_commons_node_2 = require("pip-services3-commons-node");
const pip_services3_rpc_node_1 = require("pip-services3-rpc-node");
class BoardsOperationsV1 extends pip_services3_rpc_node_1.RestOperations {
    constructor() {
        super();
        this._correlationId = "boards-operations";
        this._dependencyResolver.put('ads-client', new pip_services3_commons_node_2.Descriptor('ad-board-ads', 'client', '*', '*', '1.0'));
        this._dependencyResolver.put('devices-client', new pip_services3_commons_node_2.Descriptor('ad-board-devices', 'client', '*', '*', '1.0'));
        this._dependencyResolver.put('device_groups-client', new pip_services3_commons_node_2.Descriptor('ad-board-devicegroups', 'client', '*', '*', '1.0'));
    }
    setReferences(references) {
        super.setReferences(references);
        this._adsClient = this._dependencyResolver.getOneRequired('ads-client');
        this._devicesClient = this._dependencyResolver.getOneRequired('devices-client');
        this._devicegroupsClient = this._dependencyResolver.getOneRequired('device_groups-client');
    }
    getAds(req, res) {
        let reqId = req.route.params.dev_id;
        let result = [];
        let device;
        let groups;
        async.series([
            (callback) => {
                this._devicesClient.getDevices(this._correlationId, pip_services3_commons_node_1.FilterParams.fromTuples("request_code", reqId), null, (err, page) => {
                    if (err) {
                        callback(err);
                        return;
                    }
                    if (page == null || page.data.length == 0) {
                        err = new pip_services3_commons_node_1.NotFoundException(this._correlationId, "NOT_FOUND", "Device with request_code " + reqId + " not found")
                            .withDetails("request_code", reqId);
                        callback(err);
                        return;
                    }
                    device = page.data[0];
                    callback();
                });
            },
            // update device last connection 
            (callback) => {
                device.last_connection = new Date();
                this._devicesClient.updateDevice(this._correlationId, device, (err, item) => {
                    if (err) {
                        callback(err);
                        return;
                    }
                    if (item == null) {
                        err = new pip_services3_commons_node_1.NotFoundException(this._correlationId, "NOT_FOUND", "Device with request_code " + reqId + " not found")
                            .withDetails("request_code", reqId);
                        callback(err);
                        return;
                    }
                    device = item;
                    callback();
                });
            },
            (callback) => {
                this._devicegroupsClient.getDevicegroups(this._correlationId, pip_services3_commons_node_1.FilterParams.fromTuples("device_ids", [device.id]), null, (err, page) => {
                    if (err) {
                        callback(err);
                        return;
                    }
                    if (page == null || page.data.length == 0) {
                        err = new pip_services3_commons_node_1.NotFoundException(this._correlationId, "NOT_FOUND", "Groups with device " + device.id + " not found")
                            .withDetails("device_id", device.id);
                        callback(err);
                        return;
                    }
                    groups = page.data.map((item) => { return item.id; });
                    callback();
                });
            },
            (callback) => {
                this._adsClient.getAds(this._correlationId, pip_services3_commons_node_1.FilterParams.fromTuples("publish_group_ids", groups, 
                //"deleted", false,
                "disabled", false, "end_time_from", new Date()), null, (err, page) => {
                    if (err) {
                        callback(err);
                        return;
                    }
                    result = page.data;
                    callback();
                });
            }
        ], (err) => {
            this.sendResult(req, res)(err, { data: result });
        });
    }
}
exports.BoardsOperationsV1 = BoardsOperationsV1;
//# sourceMappingURL=BoardsOperationsV1.js.map