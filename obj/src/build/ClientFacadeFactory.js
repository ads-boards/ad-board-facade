"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClientFacadeFactory = void 0;
const pip_services3_components_node_1 = require("pip-services3-components-node");
const pip_clients_logging_node_1 = require("pip-clients-logging-node");
const ad_board_clients_ads_node_1 = require("ad-board-clients-ads-node");
const ad_board_clients_devicegroups_node_1 = require("ad-board-clients-devicegroups-node");
const ad_board_clients_devices_node_1 = require("ad-board-clients-devices-node");
const pip_clients_blobs_node_1 = require("pip-clients-blobs-node");
const ad_board_clients_yandexweather_node_1 = require("ad-board-clients-yandexweather-node");
class ClientFacadeFactory extends pip_services3_components_node_1.CompositeFactory {
    constructor() {
        super();
        this.add(new pip_clients_logging_node_1.LoggingClientFactory());
        this.add(new ad_board_clients_ads_node_1.AdsClientFactory());
        this.add(new ad_board_clients_devicegroups_node_1.DevicegroupsClientFactory());
        this.add(new ad_board_clients_devices_node_1.DevicesClientFactory());
        this.add(new pip_clients_blobs_node_1.BlobsClientFactory());
        this.add(new ad_board_clients_yandexweather_node_1.DefaultYandexWeatherFactory());
    }
}
exports.ClientFacadeFactory = ClientFacadeFactory;
//# sourceMappingURL=ClientFacadeFactory.js.map