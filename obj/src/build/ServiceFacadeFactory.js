"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServiceFacadeFactory = void 0;
const pip_services3_components_node_1 = require("pip-services3-components-node");
const pip_services_logging_node_1 = require("pip-services-logging-node");
const ad_board_services_ads_node_1 = require("ad-board-services-ads-node");
const ad_board_services_devicegroups_node_1 = require("ad-board-services-devicegroups-node");
const ad_board_services_devices_node_1 = require("ad-board-services-devices-node");
const BlobsServiceFactory_1 = require("pip-services-blobs-node/obj/src/build/BlobsServiceFactory");
class ServiceFacadeFactory extends pip_services3_components_node_1.CompositeFactory {
    constructor() {
        super();
        this.add(new pip_services_logging_node_1.LoggingServiceFactory());
        this.add(new ad_board_services_ads_node_1.AdsServiceFactory());
        this.add(new ad_board_services_devicegroups_node_1.DevicegroupsServiceFactory());
        this.add(new ad_board_services_devices_node_1.DevicesServiceFactory());
        this.add(new BlobsServiceFactory_1.BlobsServiceFactory());
    }
}
exports.ServiceFacadeFactory = ServiceFacadeFactory;
//# sourceMappingURL=ServiceFacadeFactory.js.map