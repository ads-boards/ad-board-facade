"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FacadeServiceV1 = void 0;
const pip_services3_rpc_node_1 = require("pip-services3-rpc-node");
const pip_services3_rpc_node_2 = require("pip-services3-rpc-node");
const AuthorizerV1_1 = require("./AuthorizerV1");
const LoggingOperationsV1_1 = require("../../operations/version1/LoggingOperationsV1");
const DevicegroupsOperationsV1_1 = require("../../operations/version1/DevicegroupsOperationsV1");
const DevicesOperationsV1_1 = require("../../operations/version1/DevicesOperationsV1");
const AdsOperationsV1_1 = require("../../operations/version1/AdsOperationsV1");
const BoardsOperationsV1_1 = require("../../operations/version1/BoardsOperationsV1");
const BlobsOperationsV1_1 = require("../../operations/version1/BlobsOperationsV1");
const WeatherOperationsV1_1 = require("../../operations/version1/WeatherOperationsV1");
class FacadeServiceV1 extends pip_services3_rpc_node_1.RestService {
    constructor() {
        super();
        this._aboutOperations = new pip_services3_rpc_node_2.AboutOperations();
        this._loggingOperations = new LoggingOperationsV1_1.LoggingOperationsV1();
        this._adsOperationsV1 = new AdsOperationsV1_1.AdsOperationsV1();
        this._devicegroupsOperationsV1 = new DevicegroupsOperationsV1_1.DevicegroupsOperationsV1();
        this._devicesOperationsV1 = new DevicesOperationsV1_1.DevicesOperationsV1();
        this._boardsOperationsV1 = new BoardsOperationsV1_1.BoardsOperationsV1();
        this._blobsOperationsV1 = new BlobsOperationsV1_1.BlobsOperationsV1();
        this._weatherOperationsV1 = new WeatherOperationsV1_1.WeatherOperationsV1();
        this._baseRoute = "api/v1";
    }
    configure(config) {
        super.configure(config);
        this._aboutOperations.configure(config);
        this._loggingOperations.configure(config);
        this._adsOperationsV1.configure(config);
        this._devicegroupsOperationsV1.configure(config);
        this._devicesOperationsV1.configure(config);
        this._boardsOperationsV1.configure(config);
        this._blobsOperationsV1.configure(config);
        this._weatherOperationsV1.configure(config);
    }
    setReferences(references) {
        super.setReferences(references);
        this._aboutOperations.setReferences(references);
        this._loggingOperations.setReferences(references);
        this._adsOperationsV1.setReferences(references);
        this._devicegroupsOperationsV1.setReferences(references);
        this._devicesOperationsV1.setReferences(references);
        this._boardsOperationsV1.setReferences(references);
        this._blobsOperationsV1.setReferences(references);
        this._weatherOperationsV1.setReferences(references);
    }
    register() {
        let auth = new AuthorizerV1_1.AuthorizerV1();
        // Restore session middleware
        // this.registerInterceptor('',
        //     (req, res, next) => { this._sessionsOperations.loadSession(req, res, next); });
        this.registerAdsOperations(auth);
        this.registerDevicegroupsOperations(auth);
        this.registerDevicesOperations(auth);
        this.registerBoardOperations(auth);
        this.registerBlobsOperations(auth);
        this.registerWeatherOperations(auth);
    }
    registerAdsOperations(auth) {
        // Admin tools ads Routes
        this.registerRouteWithAuth('get', '/admin/ads', null, auth.anybody(), (req, res) => { this._adsOperationsV1.getAds(req, res); });
        this.registerRouteWithAuth('get', '/admin/ads/:ad_id', null, auth.anybody(), (req, res) => { this._adsOperationsV1.getAd(req, res); });
        this.registerRouteWithAuth('post', '/admin/ads', null, auth.anybody(), (req, res) => { this._adsOperationsV1.createAd(req, res); });
        this.registerRouteWithAuth('put', '/admin/ads/:ad_id', null, auth.anybody(), (req, res) => { this._adsOperationsV1.updateAd(req, res); });
        this.registerRouteWithAuth('del', '/admin/ads/:ad_id', null, auth.anybody(), (req, res) => { this._adsOperationsV1.deleteAd(req, res); });
    }
    registerDevicegroupsOperations(auth) {
        // Admin tools device groups Routes
        this.registerRouteWithAuth('get', '/admin/devs_groups', null, auth.anybody(), (req, res) => { this._devicegroupsOperationsV1.getDeviceGroups(req, res); });
        this.registerRouteWithAuth('get', '/admin/devs_groups/:group_id', null, auth.anybody(), (req, res) => { this._devicegroupsOperationsV1.getDeviceGroup(req, res); });
        this.registerRouteWithAuth('post', '/admin/devs_groups', null, auth.anybody(), (req, res) => { this._devicegroupsOperationsV1.createDeviceGroup(req, res); });
        this.registerRouteWithAuth('put', '/admin/devs_groups/:group_id', null, auth.anybody(), (req, res) => { this._devicegroupsOperationsV1.updateDeviceGroup(req, res); });
        this.registerRouteWithAuth('del', '/admin/devs_groups/:group_id', null, auth.anybody(), (req, res) => { this._devicegroupsOperationsV1.deleteDeviceGroup(req, res); });
    }
    registerDevicesOperations(auth) {
        // Admin tools device Routes
        this.registerRouteWithAuth('get', '/admin/devs', null, auth.anybody(), (req, res) => { this._devicesOperationsV1.getDevices(req, res); });
        this.registerRouteWithAuth('get', '/admin/devs/:dev_id', null, auth.anybody(), (req, res) => { this._devicesOperationsV1.getDevice(req, res); });
        this.registerRouteWithAuth('get', '/admin/devs/:dev_id/id', null, auth.anybody(), (req, res) => { this._devicesOperationsV1.getDeviceCode(req, res); });
        this.registerRouteWithAuth('post', '/admin/devs', null, auth.anybody(), (req, res) => { this._devicesOperationsV1.createDevice(req, res); });
        this.registerRouteWithAuth('put', '/admin/devs/:dev_id', null, auth.anybody(), (req, res) => { this._devicesOperationsV1.updateDevice(req, res); });
        this.registerRouteWithAuth('del', '/admin/devs/:dev_id', null, auth.anybody(), (req, res) => { this._devicesOperationsV1.deleteDevice(req, res); });
    }
    registerBoardOperations(auth) {
        this.registerRouteWithAuth('get', '/board/ads/:dev_id', null, auth.anybody(), (req, res) => { this._boardsOperationsV1.getAds(req, res); });
    }
    registerBlobsOperations(auth) {
        // Blobs routes
        this.registerRouteWithAuth('get', '/content/blobs', null, auth.anybody(), (req, res) => { this._blobsOperationsV1.getBlobs(req, res); });
        this.registerRouteWithAuth('get', '/content/blobs/:blob_id/info', null, auth.anybody(), (req, res) => { this._blobsOperationsV1.getBlobInfo(req, res); });
        this.registerRouteWithAuth('get', '/content/blobs/:blob_id', null, auth.anybody(), (req, res) => { this._blobsOperationsV1.getBlob(req, res); });
        this.registerRouteWithAuth('post', '/content/blobs', null, auth.anybody(), (req, res) => { this._blobsOperationsV1.setBlob(req, res); });
        this.registerRouteWithAuth('post', '/content/blobs/:blob_id', null, auth.anybody(), (req, res) => { this._blobsOperationsV1.setBlob(req, res); });
        // this.registerRouteWithAuth('post', '/content/blobs/url', null, auth.anybody(),
        //     (req, res) => { this._blobsOperationsV1.loadBlobFromUrl(req, res); });
        this.registerRouteWithAuth('put', '/content/blobs/:blob_id/info', null, auth.anybody(), (req, res) => { this._blobsOperationsV1.updateBlobInfo(req, res); });
        this.registerRouteWithAuth('put', '/content/blobs/:blob_id', null, auth.anybody(), (req, res) => { this._blobsOperationsV1.setBlob(req, res); });
        // this.registerRouteWithAuth('put', '/content/blobs/:blob_id/url', null, auth.anybody(),
        //     (req, res) => { this._blobsOperationsV1.loadBlobFromUrl(req, res); });
        this.registerRouteWithAuth('del', '/content/blobs/:blob_id', null, auth.anybody(), (req, res) => { this._blobsOperationsV1.deleteBlob(req, res); });
    }
    registerWeatherOperations(auth) {
        this.registerRouteWithAuth('get', '/weather/cache', null, auth.anybody(), (req, res) => { this._weatherOperationsV1.getWeatherFromCache(req, res); });
        this.registerRouteWithAuth('get', '/weather/now', null, auth.anybody(), (req, res) => { this._weatherOperationsV1.getWeather(req, res); });
    }
}
exports.FacadeServiceV1 = FacadeServiceV1;
//# sourceMappingURL=FacadeServiceV1.js.map