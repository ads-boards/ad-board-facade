import { IReferences } from 'pip-services3-commons-node';
import { ConfigParams } from 'pip-services3-commons-node';
import { RestService } from 'pip-services3-rpc-node';
export declare class FacadeServiceV1 extends RestService {
    private _aboutOperations;
    private _loggingOperations;
    private _adsOperationsV1;
    private _devicegroupsOperationsV1;
    private _devicesOperationsV1;
    private _boardsOperationsV1;
    private _blobsOperationsV1;
    private _weatherOperationsV1;
    constructor();
    configure(config: ConfigParams): void;
    setReferences(references: IReferences): void;
    register(): void;
    private registerAdsOperations;
    private registerDevicegroupsOperations;
    private registerDevicesOperations;
    private registerBoardOperations;
    private registerBlobsOperations;
    private registerWeatherOperations;
}
