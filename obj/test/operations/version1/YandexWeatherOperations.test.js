"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let _ = require('lodash');
let async = require('async');
let assert = require('chai').assert;
const TestUsers_1 = require("../../fixtures/TestUsers");
const TestReferences_1 = require("../../fixtures/TestReferences");
const TestRestClient_1 = require("../../fixtures/TestRestClient");
suite('YandexWeatherOperationsV1', () => {
    let references;
    let rest;
    setup((done) => {
        rest = new TestRestClient_1.TestRestClient();
        references = new TestReferences_1.TestReferences();
        references.open(null, done);
    });
    teardown((done) => {
        references.close(null, done);
    });
    test('should perform yandex weather cache operations', (done) => {
        rest.getAsUser(TestUsers_1.TestUsers.User1SessionId, '/api/v1//weather/cache', (err, req, res, weather) => {
            assert.isNotNull(err);
            // assert.isObject(weather);
            done();
        });
    });
    test('should perform yandex weather operations', (done) => {
        rest.getAsUser(TestUsers_1.TestUsers.User1SessionId, '/api/v1/weather/now', (err, req, res, weather) => {
            assert.isNotNull(err);
            //  assert.isObject(weather);
            done();
        });
    });
});
//# sourceMappingURL=YandexWeatherOperations.test.js.map