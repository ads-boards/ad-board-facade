"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let _ = require('lodash');
let async = require('async');
let assert = require('chai').assert;
const TestUsers_1 = require("../../fixtures/TestUsers");
const TestReferences_1 = require("../../fixtures/TestReferences");
const TestRestClient_1 = require("../../fixtures/TestRestClient");
let GROUP1 = {
    id: '1',
    device_ids: ['1', '2'],
    description: 'Title 1',
};
let GROUP2 = {
    id: '2',
    device_ids: ['1', '3'],
    description: 'Title 2',
};
suite('DeviceGroupsOperationsV1', () => {
    let references;
    let rest;
    setup((done) => {
        rest = new TestRestClient_1.TestRestClient();
        references = new TestReferences_1.TestReferences();
        references.open(null, done);
    });
    teardown((done) => {
        references.close(null, done);
    });
    test('should perform device groups operations', (done) => {
        let dev_grp1;
        async.series([
            // Create one dev_grp
            (callback) => {
                rest.postAsUser(TestUsers_1.TestUsers.AdminUserSessionId, '/api/v1/admin/devs_groups', GROUP1, (err, req, res, dev_grp) => {
                    assert.isNull(err);
                    assert.isObject(dev_grp);
                    assert.equal(dev_grp.description, GROUP1.description);
                    dev_grp1 = dev_grp;
                    callback();
                });
            },
            // Create another dev_grp
            (callback) => {
                rest.postAsUser(TestUsers_1.TestUsers.AdminUserSessionId, '/api/v1/admin/devs_groups', GROUP2, (err, req, res, dev_grp) => {
                    assert.isNull(err);
                    assert.isObject(dev_grp);
                    assert.equal(dev_grp.description, GROUP2.description);
                    callback();
                });
            },
            // Get all dev_grps
            (callback) => {
                rest.getAsUser(TestUsers_1.TestUsers.User1SessionId, '/api/v1/admin/devs_groups', (err, req, res, page) => {
                    assert.isNull(err);
                    assert.isObject(page);
                    assert.lengthOf(page.data, 2);
                    callback();
                });
            },
            // Update the dev_grp
            (callback) => {
                dev_grp1.description = 'Updated Content 1';
                rest.putAsUser(TestUsers_1.TestUsers.AdminUserSessionId, '/api/v1/admin/devs_groups/' + dev_grp1.id, dev_grp1, (err, req, res, dev_grp) => {
                    assert.isNull(err);
                    assert.isObject(dev_grp);
                    assert.equal(dev_grp.description, 'Updated Content 1');
                    dev_grp1 = dev_grp;
                    callback();
                });
            },
            // Delete dev_grp
            (callback) => {
                rest.delAsUser(TestUsers_1.TestUsers.AdminUserSessionId, '/api/v1/admin/devs_groups/' + dev_grp1.id, (err, req, res, result) => {
                    assert.isNull(err);
                    //assert.isNull(result);
                    callback();
                });
            },
            // Try to get delete dev_grp
            (callback) => {
                rest.getAsUser(TestUsers_1.TestUsers.AdminUserSessionId, '/api/v1/admin/devs_groups/' + dev_grp1.id, (err, req, res, result) => {
                    assert.isNull(err);
                    //assert.isNull(result);
                    callback();
                });
            }
        ], done);
    });
});
//# sourceMappingURL=DevicegroupsOperations.test.js.map