"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let _ = require('lodash');
let async = require('async');
let assert = require('chai').assert;
const TestUsers_1 = require("../../fixtures/TestUsers");
const TestReferences_1 = require("../../fixtures/TestReferences");
const TestRestClient_1 = require("../../fixtures/TestRestClient");
const ad_board_clients_ads_node_1 = require("ad-board-clients-ads-node");
let now = new Date();
let AD1 = {
    id: '1',
    publish_group_ids: ['1', '2'],
    title: 'Title 1',
    text: 'Ad text 1',
    img: '',
    text_color: 'black',
    text_size: 14,
    background_color: 'white',
    critical: false,
    deleted: false,
    disabled: false,
    size: ad_board_clients_ads_node_1.AdSizeV1.Small,
    create_time: now,
    end_time: new Date(now.getTime() + 3000)
};
let AD2 = {
    id: '2',
    publish_group_ids: ['1', '3'],
    title: 'Title 2',
    text: 'Ad text 2',
    img: '',
    text_color: 'black',
    text_size: 14,
    background_color: 'white',
    critical: false,
    deleted: false,
    disabled: false,
    size: ad_board_clients_ads_node_1.AdSizeV1.Small,
    create_time: now,
    end_time: new Date(now.getTime() + 3000)
};
suite('AdminToolsAdsOperationsV1', () => {
    let references;
    let rest;
    setup((done) => {
        rest = new TestRestClient_1.TestRestClient();
        references = new TestReferences_1.TestReferences();
        references.open(null, done);
    });
    teardown((done) => {
        references.close(null, done);
    });
    test('should perform ads operations', (done) => {
        let ad1;
        async.series([
            // Create one ad
            (callback) => {
                rest.postAsUser(TestUsers_1.TestUsers.AdminUserSessionId, '/api/v1/admin/ads', AD1, (err, req, res, ad) => {
                    assert.isNull(err);
                    assert.isObject(ad);
                    assert.equal(ad.title, AD1.title);
                    ad1 = ad;
                    callback();
                });
            },
            // Create another ad
            (callback) => {
                rest.postAsUser(TestUsers_1.TestUsers.AdminUserSessionId, '/api/v1/admin/ads', AD2, (err, req, res, ad) => {
                    assert.isNull(err);
                    assert.isObject(ad);
                    assert.equal(ad.title, AD2.title);
                    callback();
                });
            },
            // Get all ads
            (callback) => {
                rest.getAsUser(TestUsers_1.TestUsers.User1SessionId, '/api/v1/admin/ads', (err, req, res, page) => {
                    assert.isNull(err);
                    assert.isObject(page);
                    assert.lengthOf(page.data, 2);
                    callback();
                });
            },
            // Update the ad
            (callback) => {
                ad1.text = 'Updated Content 1';
                rest.putAsUser(TestUsers_1.TestUsers.AdminUserSessionId, '/api/v1/admin/ads/' + ad1.id, ad1, (err, req, res, ad) => {
                    assert.isNull(err);
                    assert.isObject(ad);
                    assert.equal(ad.title, AD1.title);
                    assert.equal(ad.text, 'Updated Content 1');
                    ad1 = ad;
                    callback();
                });
            },
            // Delete ad
            (callback) => {
                rest.delAsUser(TestUsers_1.TestUsers.AdminUserSessionId, '/api/v1/admin/ads/' + ad1.id, (err, req, res, result) => {
                    assert.isNull(err);
                    //assert.isNull(result);
                    callback();
                });
            },
            // Try to get delete ad
            (callback) => {
                rest.getAsUser(TestUsers_1.TestUsers.AdminUserSessionId, '/api/v1/admin/ads/' + ad1.id, (err, req, res, result) => {
                    assert.isNull(err);
                    //assert.isNull(result);
                    callback();
                });
            }
        ], done);
    });
});
//# sourceMappingURL=AdsOperations.test.js.map