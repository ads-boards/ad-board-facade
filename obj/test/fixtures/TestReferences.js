"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TestReferences = void 0;
const _ = require('lodash');
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_commons_node_2 = require("pip-services3-commons-node");
const pip_services3_components_node_1 = require("pip-services3-components-node");
const pip_services3_container_node_1 = require("pip-services3-container-node");
const ClientFacadeFactory_1 = require("../../src/build/ClientFacadeFactory");
const ServiceFacadeFactory_1 = require("../../src/build/ServiceFacadeFactory");
const pip_services3_rpc_node_1 = require("pip-services3-rpc-node");
const FacadeServiceV1_1 = require("../../src/services/version1/FacadeServiceV1");
class TestReferences extends pip_services3_container_node_1.ManagedReferences {
    constructor() {
        super();
        this._factory = new pip_services3_components_node_1.CompositeFactory();
        this.setupFactories();
        this.appendDependencies();
        this.configureService();
        //this.createUsersAndSessions();
    }
    setupFactories() {
        this._factory.add(new ClientFacadeFactory_1.ClientFacadeFactory());
        this._factory.add(new ServiceFacadeFactory_1.ServiceFacadeFactory());
        this._factory.add(new pip_services3_rpc_node_1.DefaultRpcFactory());
    }
    append(descriptor) {
        let component = this._factory.create(descriptor);
        this.put(descriptor, component);
    }
    appendDependencies() {
        // Add factories
        this.put(null, this._factory);
        // Add service
        this.put(null, new FacadeServiceV1_1.FacadeServiceV1());
        // Add infrastructure services
        this.append(new pip_services3_commons_node_2.Descriptor('pip-services-logging', 'persistence-messages', 'memory', 'default', '*'));
        this.append(new pip_services3_commons_node_2.Descriptor('pip-services-logging', 'persistence-errors', 'memory', 'default', '*'));
        this.append(new pip_services3_commons_node_2.Descriptor('pip-services-logging', 'controller', 'default', 'default', '*'));
        this.append(new pip_services3_commons_node_2.Descriptor('pip-services-logging', 'client', 'direct', 'default', '*'));
        // Add admin tool ads
        this.append(new pip_services3_commons_node_2.Descriptor('ad-board-ads', 'persistence', 'memory', 'default', '*'));
        this.append(new pip_services3_commons_node_2.Descriptor('ad-board-ads', 'controller', 'default', 'default', '*'));
        this.append(new pip_services3_commons_node_2.Descriptor('ad-board-ads', 'client', 'direct', 'default', '*'));
        // Add admin tool devices
        this.append(new pip_services3_commons_node_2.Descriptor('ad-board-devices', 'persistence', 'memory', 'default', '*'));
        this.append(new pip_services3_commons_node_2.Descriptor('ad-board-devices', 'controller', 'default', 'default', '*'));
        this.append(new pip_services3_commons_node_2.Descriptor('ad-board-devices', 'client', 'direct', 'default', '*'));
        // Add admin tool device groups
        this.append(new pip_services3_commons_node_2.Descriptor('ad-board-devicegroups', 'persistence', 'memory', 'default', '*'));
        this.append(new pip_services3_commons_node_2.Descriptor('ad-board-devicegroups', 'controller', 'default', 'default', '*'));
        this.append(new pip_services3_commons_node_2.Descriptor('ad-board-devicegroups', 'client', 'direct', 'default', '*'));
        // Add blobs references
        this.append(new pip_services3_commons_node_2.Descriptor('pip-services-blobs', 'persistence', 'memory', 'default', '*'));
        this.append(new pip_services3_commons_node_2.Descriptor('pip-services-blobs', 'controller', 'default', 'default', '*'));
        this.append(new pip_services3_commons_node_2.Descriptor('pip-services-blobs', 'client', 'direct', 'default', '*'));
        // Yandex weather
        this.append(new pip_services3_commons_node_2.Descriptor('ad-board-yandexweather', 'client', 'direct', 'default', '*'));
    }
    configureService() {
        // Configure Facade service
        let serviceWether = this.getOneRequired(new pip_services3_commons_node_2.Descriptor('ad-board-yandexweather', 'client', 'direct', 'default', '*'));
        serviceWether.configure(pip_services3_commons_node_1.ConfigParams.fromTuples('credential.access_key', " ", 'options.update_interval', 1000, // 1 sec
        'options.lat', '55.75396', 'options.lon', '37.620393'));
        // Configure Facade service
        let service = this.getOneRequired(new pip_services3_commons_node_2.Descriptor('pip-services', 'endpoint', 'http', 'default', '*'));
        service.configure(pip_services3_commons_node_1.ConfigParams.fromTuples('root_path', '', //'/api/v1',
        'connection.protocol', 'http', 'connection.host', '0.0.0.0', 'connection.port', 3000));
    }
}
exports.TestReferences = TestReferences;
//# sourceMappingURL=TestReferences.js.map