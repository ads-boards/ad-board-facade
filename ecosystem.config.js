module.exports = {
    apps: [
      {
        name: 'ads-facade',
        script: './bin/run.js',
        env: {
          NODE_ENV: 'development',
          YANDEX_API_KEY:''
        },
        env_production: {
          NODE_ENV: 'production',
          YANDEX_API_KEY:''
        },
      },
    ],
  }